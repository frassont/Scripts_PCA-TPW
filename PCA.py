#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 10:37:01 2024

@author: thomas
"""

import os
import h5py
import shtns
import numpy as np
import argparse
import sys
import matplotlib.pyplot as plt
try:
    import cartopy.crs as ccrs
except:
    print('Cartopy not found')


###############################################################################
def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("path_to_dir" ,
                        help = "path to the data directory",
                        type=str)
    parser.add_argument("lmax" , 
                        help = "Maximum SH degree to consider",
                        type=int)
    parser.add_argument("model" , 
                        help = "Model used (MF or MC)",
                        type=str)
    args = parser.parse_args()
    return(args)
###############################################################################


###############################################################################
def read_file(path, model):
    """
    Read a file and extract the data
    """
    file = h5py.File(path, 'r')
    data = file['data'][:]
    file.close()
    if model == 'MF':
        data = np.flip(data, axis=0)
    return(data)
###############################################################################


###############################################################################
def build_matrix(files, model):
    """
    Build the matrix to be decomposed in singular value. 
    The lines of the matrix are the SH decomposition of the snapshots.

    """
    print('building of the data matrix...')
    A = np.zeros((len(files),(lmax+1)**2))
    times = np.zeros(len(files))
    #the matrix is filled line by line
    for k in range(len(files)):
        time = -int(files[k][-7:-3])
        times[k] = time
        #import the snapshot
        data = read_file(path + '/' + files[k], model)
        #spherical harmonic decomposition using shtns
        coeffs = sh.analys(data) 
        #real and imaginary part are decomposed
        #real part
        Rcoeffs = coeffs.real
        #imaginary part
        Icoeffs = coeffs[lmax + 1:].imag
        #the matrix is filled
        A[k] = np.concatenate((Rcoeffs,Icoeffs))
    print('Done')
    print("----------")
    return(A, times)
###############################################################################


###############################################################################
def reshape_coeffs(coeffs,lmax):
    """
    Build an array containing the coefficients of a SH decomposition with the 
    shtns definition from a line of the decomposed matrix

    """
    #if the mean has been removed, add the l=m=0 coeff = 0
    if len(coeffs) == (lmax+1) * (lmax+2) - lmax - 2:
        coeffs = np.concatenate((np.array([0]), coeffs))
    #number of SH coeffs
    nbcoeffs = int((lmax+1)*(lmax+2)/2)
    #real part of the coefficients
    Rcoeffs = coeffs[:nbcoeffs]
    #imaginary part of the coefficients
    Icoeffs = coeffs[nbcoeffs:] 
    #initialization of the array
    coeffs_shaped = np.zeros((nbcoeffs), dtype='complex')
    #l=0 coefficients
    coeffs_shaped[:lmax + 1] = Rcoeffs[:lmax + 1] + 0j 
    #l!=0 coefficients
    coeffs_shaped[lmax + 1:] = Rcoeffs[lmax + 1:] + Icoeffs*1j
    return(coeffs_shaped)
###############################################################################


###############################################################################
def compute_PCA(A):
    """
    Compute the PCA of a data matrix A. Returns the average pattern g, the 
    weights L, the singular values S and the patterns R

    """
    # "gravity center" of the data, i.e. averaged pattern
    g = np.dot(A.T, 1/A.shape[0] * np.ones(A.shape[0]))
    # centering of the data
    A -= np.outer(np.ones(A.shape[0]), g.T)
    #decomposition in singular values
    svd = np.linalg.svd(A, full_matrices = False)
    L,S,R = svd[0],svd[1],svd[2]
    print("PCA computed")
    print("----------")
    return(g, L, S, R)
###############################################################################


###############################################################################
def plot_amplitudes(L,S,time,components=[1,2,3]):
    """
    Plot the amplitudes as a function of time of given components.

    """
    fig = plt.figure(figsize=(7,4.8))
    ax = fig.add_subplot(111)
    for k in components:
        ax.plot(time,L[:,k-1]*S[k-1],linewidth=2,label=f"$k = {k}$")
    plt.tick_params(labelsize=20)
    plt.xlabel('Time (Myr)',fontsize=25)
    plt.ylabel(r'$w_k(t) \times s_k$ (mW m$^{-2}$)',fontsize=25)
    plt.minorticks_on()
    plt.tick_params(right=True,which='both')
    plt.xlim(np.min(time),np.max(time))
    ax.grid()
    ax.grid(which='minor',linestyle='--',alpha=0.5)
    lg = plt.legend(fontsize=15,loc='best')
    lg.set_alpha(0.3)
    plt.tight_layout()
    return(fig)
###############################################################################


###############################################################################
def plot_pattern(R, component=1):
    """
    Plot the pattern of a given component.

    """
    coeffs = reshape_coeffs(R[component-1], lmax)
    pattern = sh.synth(coeffs)
    maxi = np.max(np.abs(pattern))
    dp = 2*np.pi/pattern.shape[1] #longitude step
    dt = np.pi/pattern.shape[0] #latitude step
    phi = np.arange(-np.pi + dp/2, np.pi , dp)*180/np.pi
    tta = -np.arange(-np.pi/2 + dt/2, np.pi/2, dt)*180/np.pi
    fig = plt.figure(figsize=(7,4))
    ax = fig.add_subplot(1,1,1,projection = ccrs.Mollweide())
    p = ax.pcolormesh(phi, tta, pattern, transform=ccrs.PlateCarree(),
                      shading='nearest', cmap='twilight_shifted',
                      vmin=-maxi, vmax=maxi)
    ax.gridlines(linestyle='--',color='black',alpha=0.5)
    plt.colorbar(p, label=r'$\tilde{p}_'+str(component)+'(\lambda, \phi)$')
    return(fig)
###############################################################################


if __name__ == '__main__':
    args = get_arguments()
    path = args.path_to_dir
    lmax = args.lmax
    #initialize sh transform
    sh = shtns.sht(lmax)
    if args.model == 'MC':
        nlat, nphi = sh.set_grid(512, 1024, shtns.sht_reg_fast)
    elif args.model == 'MF':
        nlat, nphi = sh.set_grid(719, 1440, shtns.sht_reg_fast)
    #list of files in the directory
    files = os.listdir(path)
    #build the data matrix
    A, times = build_matrix(files, args.model)
    #compute the PCA
    g, L, S, R = compute_PCA(A)
    #plot the amplitudes of the three first components
    fig_amp = plot_amplitudes(L, S, times)
    plt.show()
    #plot the pattern of the firt component
    fig_pat = plot_pattern(R, 1)
    plt.show()
    