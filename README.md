Scripts used in Frasson et al. (2024) to compute the principal component analysis (PCA) and the true polar wander (TPW). The data used by these scripts can be found here: https://doi.org/10.5281/zenodo.10523321. The script "PCA.py" computes the PCA of the field stored in the path provided as argument for the given model. The script "inertia.py" computes the successive positions of the inertia axis, and includes funcitons to rotate the outputs according to the positions of the inertia axis.

## Usage
The scripts can be used in terminals.
- To compute the PCA of the CMB heat flux for case MF1: python PCA.py *path_to_supplementary*/MC/MC1/qcmb MC
- To compute the inertia axes for model MF with the No LVVs geoid: python inertia.py *path_to_supplementary*/MF/MF0/geoid_nolvv MF

