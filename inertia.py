#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 12:12:07 2024

@author: thomas
"""

import numpy as np
import h5py
import matplotlib.pyplot as plt
import shtns
import argparse
import os
import pandas as pd
import scipy as sp
import cartopy.crs as ccrs

###############################################################################
def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("path_to_dir" ,
                        help = "path to the data directory",
                        type=str)
    parser.add_argument("model" , 
                        help = "Model used (MF or MC)",
                        type=str)
    args = parser.parse_args()
    return(args)
###############################################################################


###############################################################################
def read_file(path, model):
    """
    Read a file and extract the data
    """
    file = h5py.File(path, 'r')
    data = file['data'][:]
    time = -file.attrs['Time (Myr)']
    file.close()
    if model == 'MF':
        data = np.flip(data,axis=0)
    else:
        time -= 300
    return(data, time)
###############################################################################


###############################################################################
def complex_to_real(coeffs,liste_l,liste_m):
    """
    Convert complex sh coefficients (shtns data structure) to real sh
    coefficients (pyshtools data structure)

    """
    #maximum degree of the SH decomposition
    deg_max = liste_l.max()
    #initialization of the SH decomposition array in the pyshtools structure
    coeffs_real = np.zeros((2,deg_max + 1,deg_max + 1)) 
    #fill the SH decompostion array with the real coefficients
    for k in range(len(coeffs)):
        #degree of the coeff
        l = liste_l[k]
        #order of the coeff
        m = liste_m[k]
        if m == 0:
            #if m=0, the real coeff is the real part of the complex coeff
            coeffs_real[0,l,m] = coeffs[k].real
        elif m > 0:
            #if m!=0, multiply by sqrt(2)
            #for m>0, use the real part of the coeff
            coeffs_real[0,l,m] = np.sqrt(2)*coeffs[k].real 
            #for m<0, use the imaginary part of the complex conjugate
            coeffs_real[1,l,m] = - np.sqrt(2)*coeffs[k].imag
    return(coeffs_real)
###############################################################################



###############################################################################
def build_inertia(clm, M, a):
    '''
    Build the non-hydrostatic inertia tensor from the degree 2 geoid 
    coefficients (clm), the mas of the Earth (M), and the radius of the
    Earth (a).
    
    '''
    c20 = clm[0,2,0]
    c21 = clm[0,2,1] 
    c22 = clm[0,2,2]
    s21 = clm[1,2,1] 
    s22 = clm[1,2,2] 
    I = np.array([[c20/np.sqrt(3) - c22, -s22, -c21],
                  [-s22, c22 + c20/np.sqrt(3), -s21],
                  [-c21, -s21, -2*c20/np.sqrt(3)]]) * M * a**2 * np.sqrt(5/3)
    return(I)
###############################################################################


###############################################################################
def inertia_axes(I, P_old):
    '''
    Diagonalize the inertia tensor (I) to obtain the inertia moments (D), 
    the latitudes (lats) and longitudes (lons) of the inertia axes, 
    and the diagonalized inertia tensor (P).
    
    '''
    D, P = sp.linalg.eig(I)
    D = np.real(D)
    indx = D.argsort()
    D = D[indx]
    P = P[:,indx]
    P[:,0] *= np.sign(np.sum(P[:,0]*P_old[:,0]))
    P[:,1] *= np.sign(np.sum(P[:,1]*P_old[:,1]))
    P[:,2] *= np.sign(np.sum(P[:,2]*P_old[:,2]))   
    lats = np.arctan(P[2]/np.sqrt(P[0]**2 + P[1]**2))*180/np.pi
    lons = np.arctan2(P[1],P[0]) *180/np.pi
    return(D, lats, lons, P)
###############################################################################


###############################################################################
def plot_axes(axes):
    '''
    Plot the latitude, longitude and moments of the inertia axes.
    
    '''
    fig=plt.figure(figsize=(12,15))
    plt.subplot(311)
    plt.plot(axes['time'].iloc[0], 
             axes['latitude'].iloc[0],label='$A$',linewidth=2)
    plt.plot(axes['time'].iloc[0], 
             axes['latitude'].iloc[1],label='$B$',linewidth=2)
    plt.plot(axes['time'].iloc[0], 
             axes['latitude'].iloc[2],label='$C$',linewidth=2)
    plt.legend(loc=(9/1131,0.673))
    plt.minorticks_on()
    plt.grid()
    plt.ylabel('Latitude (°)',fontsize=12)
    plt.xlim(axes['time'].iloc[0].min(), axes['time'].iloc[0].max())
    plt.ticklabel_format(axis='y', style='sci',scilimits=(0,0))
    plt.subplot(312)
    plt.plot(axes['time'].iloc[0], 
             axes['longitude'].iloc[0],label='$A$',linewidth=2)
    plt.plot(axes['time'].iloc[0], 
             axes['longitude'].iloc[1],label='$B$',linewidth=2)
    plt.plot(axes['time'].iloc[0], 
             axes['longitude'].iloc[2],label='$C$',linewidth=2)
    plt.legend(loc=(9/1131,0.673))
    plt.minorticks_on()
    plt.grid()
    plt.ylabel('Longitude (°)',fontsize=12)
    plt.xlim(axes['time'].iloc[0].min(), axes['time'].iloc[0].max())
    plt.ticklabel_format(axis='y', style='sci',scilimits=(0,0))
    plt.subplot(313)
    plt.plot(axes['time'].iloc[0], 
             axes['moment'].iloc[0],label='$A$',linewidth=2)
    plt.plot(axes['time'].iloc[0],
             axes['moment'].iloc[1],label='$B$',linewidth=2)
    plt.plot(axes['time'].iloc[0], 
             axes['moment'].iloc[2],label='$C$',linewidth=2)
    plt.legend(loc=(9/1131,0.673))
    plt.minorticks_on()
    plt.grid()
    plt.xlabel('Time (Ma)', fontsize=20)
    plt.ylabel('Eignevalues',fontsize=12)
    plt.xlim(axes['time'].iloc[0].min(), axes['time'].iloc[0].max())
    plt.ticklabel_format(axis='y', style='sci',scilimits=(0,0))
    plt.tight_layout()
    plt.show()
    return(fig)
###############################################################################

###############################################################################
def rotation_matrix(alpha, beta, gamma):
    '''
    Build the rotation matrix for a given set of euler angles (ZYZ convention).

    '''
    #rotation matrix of an angle alpha around the Z axis
    R_alpha = np.array([[np.cos(alpha), -np.sin(alpha), 0],
                        [np.sin(alpha), np.cos(alpha), 0],
                        [0, 0, 1]])
    #rotation matrix of an angle beta around the Y axis
    R_beta = np.array([[np.cos(beta), 0, np.sin(beta)],
                      [0, 1, 0],
                      [- np.sin(beta), 0, np.cos(beta)]])
    #rotation matrix of an angle gamma around the Z axis
    R_gamma = np.array([[np.cos(gamma), -np.sin(gamma), 0],
                        [np.sin(gamma), np.cos(gamma), 0],
                        [0, 0, 1]])
    #Compute the product of the three rotation matrix
    R_tot = R_gamma @ R_beta @ R_alpha
    return(R_tot)
###############################################################################


###############################################################################
def cartesian(lat, lon):
    '''
    Compute the cartesian coordinates associated with spherical coordinates
    on a unit sphere.

    '''
    x = np.cos(lat)*np.cos(lon)
    y = np.cos(lat)*np.sin(lon)
    z = np.sin(lat)
    return(x, y, z)
###############################################################################

###############################################################################
def spherical(x, y, z):
    '''
    Compute the spherical coordinates associated with cartesian coordinates

    '''
    theta = np.arcsin(z)
    phi = np.arctan2(y,x)
    return(theta, phi)
###############################################################################

###############################################################################
def rotation_coord(coord, euler):
    '''
    Compute the rotation of spherical coordinates (coord), 
    given a set of euler angles (euler).

    '''
    theta, phi = coord
    alpha, beta, gamma = euler
    #conversion to cartesian coordinates
    x, y, z = cartesian(theta, phi)
    #computation of the rotation matrix
    R = np.linalg.inv(rotation_matrix(alpha, beta, gamma))
    #computation of the rotated cartesian coordinates
    xr, yr, zr = np.dot(R,np.array([x,y,z]))
    #conversion to spherical coordinates
    theta_r, phi_r = spherical(xr,yr,zr)
    coord_r = [theta_r,phi_r]
    return(coord_r)
###############################################################################

###############################################################################
def sequential_coordinates(coord_imax):
    '''
    Compute the sequential coordinate from a list of coordinate.
    At each time step, the sequential coordinates give the position at time t
    relative to the position at time t-dt0
    
    '''
    #initialize the sequential coordinates of the main inertia axis
    coord_seq = np.zeros((len(coord_imax),2)) 
    coord_imax = np.flip(coord_imax, axis=0)
    coord_seq[0] = np.copy(coord_imax[0]) * pi/180
    #loop over all the snapshots
    for k in range(1,len(coord_seq)):
        #latitude and longitude of the inertia axis in the simulation frame
        lat, lon = np.copy(coord_imax[k]) * pi/180
        #initialization of the new coordinates after rotation
        coord = np.array([lat, lon])
        #loop over previous time steps
        for l in range(k):
            #definition of the euler angles
            alpha = -  coord_seq[l,1] #rotation about axis Z
            beta = pi/2 - coord_seq[l,0]#rotation about axis Y
            gamma = + coord_seq[l,1] #rotation about axis Z
            euler = [alpha, beta, gamma]
            #l-th rotation of the k-th coordinates
            theta_r, phi_r = rotation_coord(coord,euler)
            coord = [theta_r,phi_r]
        coord_seq[k] = (theta_r, phi_r)
    return(np.flip(coord_seq,axis=0))
###############################################################################


###############################################################################
def rotation_matrix_angles(angles):
    '''
    Build the rotation matrix for a given set of euler angles (ZYZ convention)

    '''
    cos = np.cos
    sin = np.sin
    a, b, c = angles[0], angles[1], angles[2]
    c1, c2, c3 = cos(a), cos(b), cos(c)
    s1, s2, s3 = sin(a), sin(b), sin(c)
    R = np.array([[c1*c2*c3-s1*s3, -c3*s1-c1*c2*s3, c1*s2],
                  [c1*s3+c2*c3*s1,  c1*c3-c2*s1*s3, s1*s2],
                  [-c3*s2,          s2*s3,          c2]])
    return(R)
###############################################################################

###############################################################################
def proper_euler_ZYZ(R):
    '''
    Compute the proper euler angles (ZYZ convention) from a rotation matrix.
    
    '''
    R11 = R[0,0]
    R21 = R[1,0]
    R13 = R[0,2]
    R22 = R[1,1]
    R23 = R[1,2]
    R31 = R[2,0]
    R32 = R[2,1]
    R33 = R[2,1]
    
    gamma = np.arctan2(R[1,2],R[0,2])
    beta = np.arctan2(np.sqrt(1-R[2,2]**2),R[2,2])
    alpha = np.arctan2(R[2,1],-R[2,0])
    return(np.array([alpha, beta, gamma]))
###############################################################################

###############################################################################
def integrate_euler(euler_seq):
    '''
    Integrate a list of sequential rotations decribed by their euler angles. 
    Return the Euler angles of the integrated rotations at each time step.
    
    '''
    euler_int = np.zeros(euler_seq.shape)
    rot_mat = np.diag((1,1,1))
    print('Integrate Euler angles...')
    for k in range(len(euler_seq)):
        alpha, beta, gamma = euler_seq[k,0], euler_seq[k,1], euler_seq[k,2]
        R = rotation_matrix(alpha, beta, gamma)
        rot_mat = np.dot(R,rot_mat)
        euler_int[k] = proper_euler_ZYZ(rot_mat)
    print('Done')
    return(euler_int)
###############################################################################

###############################################################################
def get_euler(axes, model):
    coord_imax = np.asarray(list(zip(axes.iloc[2]['latitude'], 
                                             axes.iloc[2]['longitude'])))
    if model == 'MC':
        coord_imax = np.flip(coord_imax, axis=0)
    coord_seq = sequential_coordinates(coord_imax)
    euler_seq = np.array([-coord_seq[:,1], np.pi/2- coord_seq[:,0], coord_seq[:,1]]).T
    euler_seq = np.flip(euler_seq, axis=0)
    euler_int = integrate_euler(np.array(euler_seq))
    if model == 'MF':
        euler_int = np.flip(euler_int, axis=0)
    return(np.flip(euler_int,axis=0))
###############################################################################

###############################################################################
def apply_rotation(data, euler, lmax=200):
    '''
    Apply the rotation to a data field (data) 
    for a given set of euler angles (euler).
    
    '''
    print(f"Rotations Z: {-euler[2]*180/np.pi}°, Y: {euler[1]*180/np.pi}°," 
          f"Z: {-euler[0]*180/np.pi}°")
    sh = shtns.sht(lmax)
    nlat, nphi = sh.set_grid(data.shape[0], data.shape[1])
    rot = shtns.rotation(lmax)
    rot.set_angles_ZYZ(euler[0], euler[1], euler[2])
    zlm = sh.analys(data)
    rlm = rot.apply_real(zlm)
    return(sh.synth(rlm))
###############################################################################

###############################################################################
def rotation(path, axes, model):
    '''
    Function to rotate a given snapshot located at a given path, 
    given the successive positions of the inertia axes (axes).
    
    '''
    euler_int = get_euler(axes, model)
    data, time = read_file(path, model)
    if model =='MC':
        it = -(time+1)
    else:
        it = -(time//5+1)
        # euler_int = np.flip(euler_int,axis=0)
    data_rot = apply_rotation(data, euler_int[it])
    return(data_rot)
###############################################################################

###############################################################################
def compute_inertia(path):
    '''
    Compute the inertia axes from geoid files stored in path.
    
    '''
    files = os.listdir(path)
    axes = pd.DataFrame(index=(1,2,3),
                        columns=('axe', 'latitude', 'longitude', 'moment'))
    axes['axe'] = ('a', 'b', 'c')
    axes['latitude'] = [np.zeros(len(files)), 
                        np.zeros(len(files)), 
                        np.zeros(len(files))]
    axes['longitude'] = [np.zeros(len(files)), 
                         np.zeros(len(files)), 
                         np.zeros(len(files))]
    axes['moment'] = [np.zeros(len(files)),
                      np.zeros(len(files)),
                      np.zeros(len(files))]
    axes['time'] = [np.zeros(len(files)),
                      np.zeros(len(files)),
                      np.zeros(len(files))]
    
    P = np.diag((1,1,1))
    print("Compute inertia...")
    for k in range(len(files)):
        it = k
        geoid, time = read_file(path + '/' + files[k], args.model)
        clm = complex_to_real(sh.analys(geoid/a), sh.l, sh.m)
        I = build_inertia(clm, M, a)
        D, lats, lons, P = inertia_axes(I, P)
        axes['moment'].iloc[0][it] = D[0]
        axes['moment'].iloc[1][it] = D[1]
        axes['moment'].iloc[2][it] = D[2]
        axes['latitude'].iloc[0][it] = lats[0]
        axes['latitude'].iloc[1][it] = lats[1]
        axes['latitude'].iloc[2][it] = lats[2]
        axes['longitude'].iloc[0][it] = lons[0]
        axes['longitude'].iloc[1][it] = lons[1]
        axes['longitude'].iloc[2][it] = lons[2]
        axes['time'].iloc[0][it] = -time
        axes['time'].iloc[1][it] = -time
        axes['time'].iloc[2][it] = -time
    if args.model == 'MC':
        axes['time'].iloc[0] = np.flip(axes['time'].iloc[0], axis=0)
        axes['time'].iloc[1] = np.flip(axes['time'].iloc[1], axis=0)
        axes['time'].iloc[2] = np.flip(axes['time'].iloc[2], axis=0)
    print("Done")
    print("----------")
    return(axes)
###############################################################################

###############################################################################
def plot_map(field, cmap='seismic', projection=ccrs.Mollweide):
    '''
    Plot a field using Cartopy.
    
    '''
    maxi = np.max(np.abs(field))
    dp = 2*np.pi/field.shape[1] #longitude step
    dt = np.pi/field.shape[0] #latitude step
    phi = np.arange(-np.pi + dp/2, np.pi , dp)*180/np.pi
    tta = -np.arange(-np.pi/2 + dt/2, np.pi/2, dt)*180/np.pi
    fig = plt.figure(figsize=(7,4))
    ax = fig.add_subplot(1,1,1,projection = ccrs.Mollweide())
    p = ax.pcolormesh(phi, tta, field, transform=ccrs.PlateCarree(),
                      shading='nearest', cmap=cmap,
                      vmin=-maxi, vmax=maxi)
    ax.gridlines(linestyle='--',color='black',alpha=0.5)
    plt.colorbar(p)
    return(fig)
###############################################################################


if __name__=='__main__':
    args = get_arguments()
    path = args.path_to_dir
    #initialize sh transform
    sh = shtns.sht(2, norm = shtns.sht_fourpi)
    if args.model == 'MC':
        nlat, nphi = sh.set_grid(512, 1024, shtns.sht_reg_fast)
    elif args.model == 'MF':
        nlat, nphi = sh.set_grid(719, 1440, shtns.sht_reg_fast)
    #define pi
    pi = np.pi
    #physical values
    M = 5.972e24 #mass of the earth
    a = 6371e3 #radius of the earth
    #compute the inertia axes
    axes = compute_inertia(path)
    fig = plot_axes(axes)
    plt.show()
        
    
    
    